// //nomor 1

function range(startNum, finishNum){
    
    temp = []

    if (finishNum == null){
        temp.push(-startNum)
    }else if (startNum == null && finishNum == null){
        temp.push(-1)
    }else{
        if (startNum < finishNum){
            for(var i = startNum;i<=finishNum;i++){
                temp.push(i)
            }
        }else if (startNum > finishNum){
            for(var i = startNum;i>=finishNum;i--){
                temp.push(i)
            }
        }
    }

    return temp
}

console.log(range(5,2))
console.log("\n")

//nomor 2

function  rangeWithStep(startNum, finishNum, step){
    
    temp = []

    if (finishNum == null){
        temp.push(-startNum)
    }else if (startNum == null && finishNum == null){
        temp.push(-1)
    }else{
        if (step == null){
            temp = range(startNum,finishNum)
        }else{
            if (startNum < finishNum){
                for(var i = startNum;i<=finishNum;i+=step){
                    temp.push(i)
                }
            }else if (startNum > finishNum){
                for(var i = startNum;i>=finishNum;i-=step){
                    temp.push(i)
                }
            }
        }
    }

    return temp
}

console.log(rangeWithStep(3,2))
console.log("\n")

//nomor 3

function sum(startNum, finishNum, step){

    if (startNum == null && finishNum == null){
        sum = 0
    }else if (finishNum == null){
        sum = 1
    }else{
        temp = rangeWithStep(startNum, finishNum, step)

        sum = temp[0]
        for(var i = 1;i<temp.length;i++){
            sum+=temp[i]
        }
    }
    return sum
}

console.log(sum())
console.log("\n")

//nomor 4

function dataHandling(data){
    for (var i = 0;i<data.length;i++){
        console.log('Nomor ID:  '+data[i][0])
        console.log('Nama Lengkap:  '+data[i][1])
        console.log('TTL:  '+data[i][2]+' '+data[i][3])
        console.log('Hobi:  '+data[i][4])
        console.log("\n")
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input)

//nomor 5

function balikKata(word){
    var kata = ""
    var i = word.length
    while (i>0){
        kata += word[i-1]
        i-- 
    }
    return kata
}

console.log(balikKata("Kasur Rusak"))
console.log("\n")

//nomor 6

function cekbulan(bulan){
    var nama_bulan = ""
    switch(bulan) {
    case 1:   { nama_bulan = 'Januari'; break; }
    case 2:   { nama_bulan = 'Februari'; break; }
    case 3:   { nama_bulan =' Maret'; break; }
    case 4:   { nama_bulan =' April'; break; }
    case 5:   { nama_bulan =' Mei'; break; }
    case 6:   { nama_bulan =' Juni'; break; }
    case 7:   { nama_bulan =' Juli'; break; }
    case 8:   { nama_bulan =' Agustus'; break; }
    case 9:   { nama_bulan =' September'; break; }
    case 10:   { nama_bulan =' Oktober'; break; }
    case 11:   { nama_bulan =' November'; break; }
    case 12:   { nama_bulan =' Desember'; break; }
    default:  {nama_bulan ='nomor bulan tidak teridentifikasi'; }}
  
    return nama_bulan
  }
  
function dataHandling2(data){
    data.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    data.splice(4,1,"Pria", "SMA Internasional Metro")
    console.log(data)

    var date = data[3].split("/")
    console.log(cekbulan(parseInt(date[1])))
    var sort_date = date.sort(function (value1, value2) { return value2 - value1 } )
    console.log(sort_date)
    console.log(date.join('-'))
    console.log(data[1].slice(0,15))
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
dataHandling2(input)

