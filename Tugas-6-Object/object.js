//nomor 1

var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(input_array){
    var temp = {}
    for(var i = 0;i<input_array.length;i++){
        temp.firstName = input_array[i][0];
        temp.lastName = input_array[i][1];
        temp.gender = input_array[i][2];
        if (input_array[i][3] > thisYear){
            temp.age = "Invalid Birth Year";
        }else if (input_array[i][3] == null){
            temp.age = "Invalid Birth Year";
        }else{
            temp.age = thisYear-input_array[i][3];
        }
        
        var consoleText = (i + 1) + '. ' + temp.firstName + ' ' + temp.lastName + ': ';
        console.log(consoleText) 
        console.log(temp)
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
console.log("\n")

//nomor 2

function shoppingTime(memberId, money) {
    
    var sale = [['Sepatu Stacattu', 1500000],['Baju Zoro', 500000],['Baju H&N', 250000],['Sweeter Uniklooh', 175000],['Casing Handphone', 50000]]

    if (memberId == null){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if (memberId == ''){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else{

        var shopping = {}
        shopping.memberId = memberId
        shopping.money = money
        
        min_harga = sale[4][1]

        var listPurchased = []
        var changeMoney = shopping.money
        var i = 0
        cek = true
        
        if (shopping.money > min_harga){
            while (cek == true){
                if (i < sale.length){
                    if (shopping.money > sale[i][1]){
                        listPurchased.push(sale[i][0])
                        changeMoney = changeMoney - sale[i][1]
                        i++
                    }else{
                        if(shopping.money < min_harga){
                            cek = false
                        }else{
                            i++
                        }
                    }
                }else{
                    cek = false
                }
            }
    
            shopping.listPurchased = listPurchased
            shopping.changeMoney = changeMoney
    
            return shopping
        }else{
            return "Mohon maaf, uang tidak cukup"
        }
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log("\n")

//nomor 3

function naikAngkot(arrPenumpang) {
    
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arr = []

    for (var i=0;i<arrPenumpang.length;i++){
        var temp = {}
        temp.penumpang = arrPenumpang[i][0]
        temp.naikDari = arrPenumpang[i][1]
        temp.tujuan = arrPenumpang[i][2]
    
        var dari = rute.indexOf(temp.naikDari)
        var tujuan = rute.indexOf(temp.tujuan)
    
        var jarak = tujuan - dari

        temp.bayar = jarak*2000
        arr.push(temp)
    }

    return arr
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]))
console.log("\n")
