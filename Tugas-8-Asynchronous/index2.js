let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

time = 10000

function baca(time,books){
    readBooksPromise(time,books[0])
        .then(function(fulfilled){
            var i = 1
            while(i<books.length){
                readBooksPromise(fulfilled,books[i])
                i++
            }
        })
        .catch(function(error){
            return error
        })
}

baca(time,books)