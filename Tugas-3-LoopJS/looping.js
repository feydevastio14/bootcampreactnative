//nomor 1

var i = 1
console.log("LOOPING PERTAMA")
while(i <= 10){
    console.log(i*2+" - I love coding")
    i++;
}

console.log("\n")

var i = 10
console.log("LOOPING KEDUA")
while(i >= 1){
    console.log(i*2+" - I will become a mobile developer")
    i--;
}

console.log("\n")

//nomor 2

console.log("OUTPUT")
for(var i = 1; i <= 20; i++) {
    if (i % 2 == 1){
        if (i % 3 == 0){
            console.log(i+" - I Love Coding")
        }else{
            console.log(i+" - Santai")
        }
    }else if (i % 2 == 0){
        console.log(i+" - Berkualitas")
    }
  } 

console.log("\n")

//nomor 3

var p = 8;
var l = 4;
var out = " ";

for (var i = 1;i<=l;i++){
  for (var j = 1;j<p;j++){
    out +="#";
  }
  console.log(out)
  out = " "
}

console.log("\n")

//nomor 4

var t = 7;
var out = " ";

for (var i = 1;i<=t;i++){
  for (var j = 1;j<=i;j++){
    out +="#";
  }
  console.log(out)
  out = " "
}

console.log("\n")

//nomor 5

var p = 8;
var l = 8;
var out = " ";

for (var i = 1;i<=l;i++){
  if(i%2 == 1){
    for (var j = 1;j<=l;j++){
      if(j%2 == 1){
        out +=" ";
      }else{
        out +="#";
      }
    }
  }else{
    for (var j = 1;j<=l;j++){
      if(j%2 == 1){
        out +="#";
      }else{
        out +=" ";
      }
    }
  }
  console.log(out)
  out = " "
}

console.log("\n")